'use strict';

const jwt = require('jsonwebtoken');
const config = require('../../config/environment');
const User = require('../users/users.model');
const bcrypt = require('bcryptjs');
module.exports = {
    login: (req, res) => {
        console.log(req.body);
        User.findOne({email : req.body.email},function(err, user){
            if(err){
                console.log(err);
                 return res.status(201).json({message : "Email not exist"});
            }
            var isPasswordValid = bcrypt.compareSync(req.body.password,user.password); 
            if(!isPasswordValid){
               return res.status(202).json({auth : false, token : null, message : "Incorrect email or password"});
            }else{
    
                let payload = {
                    user_id : user._id,
                    email:user.email
                }
    
                console.log(config.secrets.session);
                let token = jwt.sign(payload, config.secrets.session,{
                    expiresIn : config.secrets.expiresIn
                });
                
                return res.status(200).json({auth : true, token : token, message : "User Logged In Successfully"});
            }
        });
    }
}