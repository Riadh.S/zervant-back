'use strict';

const BillSetting = require('../bill_setting/bill_setting.model');
module.exports = {
    index: (req, res) => {
        BillSetting
        .find({})
        .exec((err, BillSettingDetails)=>{
            if (err) {
                console.error(err);
                res.status(500).json({message : err})
            }
            res.status(200).json({ message: "BillSetting Details fetched Successfully", data : BillSettingDetails});
        })
    },
    retrieve: (req, res) => {
        const BillSettingId = req.params.id;
        BillSetting
        .findOne({_id:BillSettingId})
        .exec((err, BillSettingDetails)=>{
            if (err) {
                console.error(err);
                res.status(500).json({message : err})
            }
            res.status(200).json({ message: "BillSetting Detail fetched Successfully", data : BillSettingDetails});
        })
    },
    retrieveByUser: (req, res) => {
        const IdUser = req.params.id;
        BillSetting
        .findOne({id_user:IdUser})
        .exec((err, BillSettingDetails)=>{
            if (err) {
                console.error(err);
                res.status(500).json({message : err})
            }
            res.status(200).json({ message: "BillSetting Detail fetched Successfully", data : BillSettingDetails});
        })
    },
    create: (req, res) => {
        console.log(req.body);
        BillSetting.create(req.body, (err, BillSettingDetails) => {
            if (err) {
                console.error(err);
                res.status(500).json({message : err})
            }
            res.status(201).json({ message: "BillSetting Created Successfully", data : BillSettingDetails});
        })
    },
    update: (req, res)=>{
        const BillSettingId = req.params.id;
        BillSetting
        .findByIdAndUpdate(BillSettingId, { $set: req.body }).exec((err, BillSettingDetails) => {
            if (err) res.status(500).json({message : err})
            res.status(200).json({ message: "BillSetting updated" });
        })
    },
    delete: (req, res)=>{
        const BillSettingId = req.params.id;
        BillSetting
        .findByIdAndDelete(BillSettingId, { $set: { is_active: false } }).exec((err, BillSettingDetails) => {
            if (err) res.status(500).json({message : err})
            res.status(200).json({ message: "BillSetting Deleted" });
        })
    }
}