'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const BillSettingSchema = new Schema({
    id_user:{
        type: String,
        required: true
    },    
    currency:{
        type: String, 
        default:'EUR'
    },
    language:{
        type: String,
        default:'English'
    },
    billHeader: {
        type: String,
        required: true
    },
    billType: {
        type: String,
        required: true
    },
    paymentCondition: {
        valuePD:{
        type: Number,
        required: true
        },
        displayPD:{
            type: Boolean,
            default:false
            }
    },
    delayInterest: {
      valueDI:{
        type: Number,
        required: true
      },
      displayDI:
      {
        type: Boolean,
        default:false
      }
    },
    paymentRef:{
        type: Boolean,
        default: false 
    },
   attentionOf:{
    type: Boolean,
    default: false 
    },
    displayColunms:
    [
       {label:String,
       value:{type:Boolean,
       default:true
    }}
    ],
    discountSetting:[
        {label:String,
            value:{type:Boolean,
            default:true
         }}
        ],
    message:{
        type: String,
        required: false 
    },
    notesFooter:{
        type: String,
        required: false 
    },
    
},{
    id: false,
    toObject: {
        virtuals: true,
        getters: true
    },
    toJSON: { 
        virtuals: true,
        getters: true, 
        setters: false 
    },
    timestamps: true
});

BillSettingSchema.pre('find', function () {
    this.where({ is_active: { $ne: false } });
});

module.exports = mongoose.model('BillSetting', BillSettingSchema);