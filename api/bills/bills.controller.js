'use strict';

const Bill = require('../bills/bills.model');
const fs= require("fs");
const PDFDocument = require('./pdfkit-tables');
const nodemailer = require("nodemailer");
const doc = new PDFDocument();
module.exports = {
    index: (req, res) => {
        Bill
        .find({})
        .exec((err, BillDetails)=>{
            if (err) {
                console.error(err);
                res.status(500).json({message : err})
            }
            res.status(200).json({ message: "Bill Details fetched Successfully", data : BillDetails});
        })
    },
    retrieve: (req, res) => {
        const BillId = req.params.id;
        Bill
        .findOne({_id:BillId})
        .exec((err, BillDetails)=>{
            if (err) {
                console.error(err);
                res.status(500).json({message : err})
            }
            res.status(200).json({ message: "Bill Detail fetched Successfully", data : BillDetails});
        })
    },
    retrieveByUser: (req, res) => {
        const IdUser = req.params.id;
        Bill
        .find({id_user:IdUser})
        .exec((err, BillDetails)=>{
            if (err) {
                console.error(err);
                res.status(500).json({message : err})
            }
            res.status(200).json({ message: "Bill Detail fetched Successfully", data : BillDetails});
        })
    },
    updatePdf: (req, res)=>{
        const BillId = req.params.id;
        Bill
        .findByIdAndUpdate(BillId, { $set: req.body }).exec((err, BillDetails) => {
            if (err) res.status(500).json({message : err})
            fs.unlink(`${__dirname}/uploads/pdf_Files/${BillId}.pdf`, (err) => {
                if (err) console.log("invoice not found");;
                console.log('path/file.pdf was deleted');
              });

              doc.pipe(fs.createWriteStream(`${__dirname}/uploads/pdf_Files/${BillDetails._id}.pdf`));
              doc.image(`api/companies/uploads/${req.body.logo}`, {
                fit: [80,80],
             });
              doc.fontSize(19).font('Helvetica-Bold').text(req.body.companyName, 300, 80);
              doc.fontSize(15).text("date created :"+req.body.billDate, 300, 120);
              doc.fontSize(15).text("due date :"+req.body.deadline, 300, 160);
              doc.fontSize(15).text(req.body.message, 100, 250);     
              let prod=[];
              let product=req.body.products;
              for(let i=0;i<product.length;i++){
                  prod[i]=[product[i].name,product[i].date,product[i].quatity,product[i].unit,product[i].priceUnit,product[i].vat,product[i].priceHT];
              }
              let row=prod;
              const table1 = {
                  
                  headers: ['Name','Date' ,'quatity', 'unit','unit Price','vat','total'],
                  
                  rows: row
              };
              
              doc.moveDown().fontSize(12).table(table1,  {
                  prepareHeader: () => doc.font('Helvetica-Bold'),
                  prepareRow: (row, i) => doc.font('Helvetica').fontSize(12)
              },20, 300, { width: 450 });
              let size=300+(product.length*50);
              doc.fontSize(10).font('Helvetica-Bold').text("total net :"+req.body.net, 400, size);
              doc.fontSize(10).font('Helvetica-Bold').text("total amount due:"+req.body.totalTTC, 400, size+30);
              doc.fontSize(10).text("payment method"+req.body.payment.paymentMeans, 100, size+60);
              doc.fontSize(10).text(req.body.payment.paypal, 150, size+80);
              doc.fontSize(10).text(req.body.notesFooter, 200, size+100);
      
      doc.end();    
            res.status(200).json({ message: "Bill updated" });
        })
    },
    update: (req, res)=>{
        const BillId = req.params.id;
        Bill
        .findByIdAndUpdate(BillId, { $set: req.body }).exec((err, BillDetails) => {
            if (err) res.status(500).json({message : err})
            res.status(200).json({ message: "Bill updated",data:BillDetails });
        })
    },
    delete: (req, res)=>{
        const BillId = req.params.id;
        Bill
        .findByIdAndDelete(BillId, { $set: { is_active: false } }).exec((err, BillDetails) => {
            if (err) res.status(500).json({message : err})
            fs.unlink(`${__dirname}/uploads/pdf_Files/${BillId}.pdf`, (err) => {
                if (err) console.log("invoice not found");;
                console.log('path/file.pdf was deleted');
              });
            res.status(200).json({ message: "Bill Deleted" });
        })
    },
    createPdfReport:(req,res)=>{
     
    },
    createpdf:(req, res) => {
        Bill.create(req.body, (err, BillDetails) => {
            if (err) {
                console.error(err);
                res.status(500).json({message : err})
            }

        doc.pipe(fs.createWriteStream(`${__dirname}/uploads/pdf_Files/${BillDetails._id}.pdf`));
        doc.image(`api/companies/uploads/${req.body.logo}`, {
            fit: [80,80],
         });
        doc.fontSize(19).font('Helvetica-Bold').text(req.body.companyName, 300, 80);
        doc.fontSize(15).text("date created :"+req.body.billDate, 300, 120);
        doc.fontSize(15).text("due date :"+req.body.deadline, 300, 160);
        doc.fontSize(15).text(req.body.message, 100, 250);     
        let prod=[];
        let product=req.body.products;
        for(let i=0;i<product.length;i++){
            prod[i]=[product[i].name,product[i].date,product[i].quatity,product[i].unit,product[i].priceUnit,product[i].vat,product[i].priceHT];
        }
        let row=prod;
        const table1 = {
            
            headers: ['Name','Date' ,'quatity', 'unit','unit Price','vat','total'],
            
            rows: row
        };
        
        doc.moveDown().fontSize(12).table(table1,  {
            prepareHeader: () => doc.font('Helvetica-Bold'),
            prepareRow: (row, i) => doc.font('Helvetica').fontSize(12)
        },20, 300, { width: 450 });
        let size=300+(product.length*60);
        doc.fontSize(10).font('Helvetica-Bold').text("total net :"+req.body.net, 400, size);
        doc.fontSize(10).font('Helvetica-Bold').text("total amount due:"+req.body.totalTTC, 400, size+30);
        doc.fontSize(10).text("payment method"+req.body.payment.paymentMeans, 100, size+60);
        doc.fontSize(10).text(req.body.payment.paypal, 150, size+80);
        doc.fontSize(10).text(req.body.notesFooter, 200, size+100);

doc.end();
      res.status(201).json({ message: "Bill Created Successfully", data : BillDetails});
    });
},
       fetchpdf:(req, res) => {
           let id=req.params.id;
        res.sendFile(`${__dirname}/uploads/pdf_Files/${id}.pdf`);
      }
}