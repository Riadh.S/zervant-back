'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const BillSchema = new Schema({
    id_user: {
        type: String,
        required: true
    },
    id_client: {
        type: String,
        required: true
    },
    companyName: {
        type: String,
        required: true
    },
    billNumber: {
        type: Number,
        required: true
    },
    billDate: {
        type: Date,
        required: true
    },
    status:{
     type:String,
     required:true
    },
    conditionPayment: {
        type: Number,
    },
 deadline: {
        type: Date,
        required:true
    },
    message:{
        type:String,
        required:false
    },
    notesFooter : {
        type: String,
       required: false
    },
    discount : {
        type: Number,
        required:false
    },
    paid:{
        type:Date,
        required:false
    },
    net: {
        type: Number,
        required:true
    },
    totalTTC: {
        type: Number,
        required:true
    },
    products:[
        {
            id_product:String,
            name:String,
            date:Date,
            quatity:Number,
            unit:String,
            priceUnit:Number,
            vat:Number,
            price:Number,
            priceHT:Number
        }
    ]
},{
    id: false,
    toObject: {
        virtuals: true,
        getters: true
    },
    toJSON: { 
        virtuals: true,
        getters: true, 
        setters: false 
    },
    timestamps: true
});

BillSchema.pre('find', function () {
    this.where({ is_active: { $ne: false } });
});

module.exports = mongoose.model('Bill', BillSchema);