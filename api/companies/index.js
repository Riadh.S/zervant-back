'use strict';
const multer = require("multer");
const express = require('express');
const controller = require('./companies.controller');
const router = express.Router();
const VerifyToken = require('../auths/verifyToken');
router.get('/upload/:id',controller.getImage); 
router.post('/upload/:id',controller.uploadImage); 
router.get('/', VerifyToken, controller.index);
router.get('/:id', VerifyToken, controller.retrieveByUser);
router.get('/:id', VerifyToken, controller.retrieve);
router.post('/', VerifyToken, controller.create);
router.put('/:id', VerifyToken, controller.update);
router.delete('/:id', VerifyToken, controller.delete);

module.exports = router;