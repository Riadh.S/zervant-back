"use strict";

const Customer = require("./customers.model");
module.exports = {
  index: (req, res) => {
    Customer.find({}).exec((err, CustomerDetails) => {
      if (err) {
        console.error(err);
        res.status(500).json({ message: err });
      }
  
      res.status(200).json({
        message: "Customer Details fetched Successfully",
        data: CustomerDetails
      });
    });
    
  },
  retrieve: (req, res) => {
    const CustomerId = req.params.id;
    Customer.findOne({ _id: CustomerId }).exec((err, CustomerDetails) => {
      if (err) {
        console.error(err);
        res.status(500).json({ message: err });
      }
      res.status(200).json({
        message: "Customer Detail fetched Successfully",
        data: CustomerDetails
      });
    });
  },
  retrieveByUser: (req, res) => {
    const IdUser = req.params.id;
    Customer.find({ id_user: IdUser }).exec((err, CustomerDetails) => {
      if (err) {
        console.error(err);
        res.status(500).json({ message: err });
      }
      res.status(200).json({
        message: "Customer Detail fetched Successfully",
        data: CustomerDetails
      });
    });
  },
  create: (req, res) => {
    console.log(req.body);
    Customer.create(req.body, (err, CustomerDetails) => {
      if (err) {
        console.error(err);
        res.status(500).json({ message: err });
      }
      res.status(201).json({
        message: "Customer Created Successfully",
        data: CustomerDetails
      });
    });
  },
  update: (req, res) => {
    const CustomerId = req.params.id;
    Customer.findByIdAndUpdate(CustomerId, { $set: req.body }).exec(
      (err, CustomerDetails) => {
        console.log(CustomerDetails);
        if (err) res.status(500).json({ message: err });
        res.status(200).json({ message: "Customer updated" });
      }
    );
  },
  delete: (req, res) => {
    const CustomerId = req.params.id;
    Customer.findByIdAndDelete(CustomerId, { $set: { is_active: false } }).exec(
      (err, CustomerDetails) => {
        console.log(CustomerDetails);
        if (err) res.status(500).json({ message: err });
        res.status(200).json({ message: "Customer Deleted" });
      }
    );
  }
};
