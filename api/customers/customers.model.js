"use strict";

const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const CustomerSchema = new Schema(
  {
    id_user: {
      type: String,
      required: true
    },
    typeOfCustomer: {
      type: String,
      required: true
    },
    companyName: {
      type: String,
      required: true
    },
    // CompanyRegistreationNumber: {
    //   type: String,
    //   required: true
    // },node app
    companyRegistrationNumber: {
      type: String,
      required: true
    },
    vatNumber: {
      type: String,
      required: true
    },
    title: {
      type: String,
      required: true
    },
    firstName: {
      type: String,
      required: true
    },
    lastName: {
      type: String,
      required: true
    },
    emailCustomer: {
      type: String,
      required: true
    },
    phone: {
      type: String,
      required: true
    },
    mobilePhone: {
      type: String,
      required: true
    },
    address: {
      type: String,
      required: true
    },
    postCode: {
      type: String,
      required: true
    },
    city: { type: String, required: true },
    country: {
      type: String,
      required: true
    },
    invoicingLanguage: {
      type: String,
      required: true
    },
    paymentTerm: {
      type: String,
      required: true
    },
    deliveryMethod: {
      type: String,
      required: true
    },
  },
  {
    id: false,
    toObject: {
      virtuals: true,
      getters: true
    },
    toJSON: {
      virtuals: true,
      getters: true,
      setters: false
    },
    timestamps: true
  }
);

CustomerSchema.pre("find", function() {
  this.where({ is_active: { $ne: false } });
});

module.exports = mongoose.model("Customer", CustomerSchema);
