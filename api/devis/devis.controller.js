'use strict';
const Bill=require('../bills/bills.model');
const Devis = require('../devis/devis.model');
const fs= require("fs");
const PDFDocument = require('../bills/pdfkit-tables');
const doc = new PDFDocument();
module.exports = {
    index: (req, res) => {
        Devis
        .find({})
        .exec((err, DevisDetails)=>{
            if (err) {
                console.error(err);
                res.status(500).json({message : err})
            }
            res.status(200).json({ message: "Devis Details fetched Successfully", data : DevisDetails});
        })
    },
    retrieve: (req, res) => {
       const DevisId = req.params.id;
        Devis
        .findOne({_id:DevisId})
        .exec((err, DevisDetails)=>{
            if (err) {
                console.error(err);
                res.status(500).json({message : err})
            }
            res.status(200).json({ message: "Devis Detail fetched Successfully", data : DevisDetails});
        })
    },
    retrieveByUser: (req, res) => {
        const IdUser = req.params.id;
        Devis
        .find({id_user:IdUser})
        .exec((err, DevisDetails)=>{
            if (err) {
                console.error(err);
                res.status(500).json({message : err})
            }
            res.status(200).json({ message: "Devis Detail fetched Successfully", data : DevisDetails});
        })
    },
    update: (req, res)=>{
        const DevisId = req.params.id;
        Devis
        .findByIdAndUpdate(DevisId, { $set: req.body }).exec((err, DevisDetails) => {
            if (err) res.status(500).json({message : err})
            res.status(200).json({ message: "Devis updated" });
        })
    },
    updatePdf: (req, res)=>{
        const DevisId = req.params.id;
        Devis
        .findByIdAndUpdate(DevisId, { $set: req.body }).exec((err, DevisDetails) => {
            if (err) res.status(500).json({message : err})
            fs.unlink(`${__dirname}/uploads/pdf_Files/${DevisId}.pdf`, (err) => {
                if (err) console.log("esstimate not found");
                console.log('path/file.pdf was deleted');
              });
        doc.pipe(fs.createWriteStream(`${__dirname}/uploads/pdf_Files/${DevisDetails._id}.pdf`));
        doc.image(`api/companies/uploads/${req.body.logo}`, {
            fit: [80,80],
         });
        doc.fontSize(19).font('Helvetica-Bold').text(req.body.companyName, 300, 80);
        doc.fontSize(15).text("date created :"+req.body.devisDate, 300, 120);
        doc.fontSize(15).text("due date :"+req.body.dateValidity, 300, 160);
        doc.fontSize(15).text(req.body.message, 100, 250);     
        let prod=[];
        let product=req.body.products;
        for(let i=0;i<product.length;i++){
            prod[i]=[product[i].name,product[i].dateProd,product[i].quatity,product[i].unit,product[i].priceUnit,product[i].vat,product[i].priceHT];
        }
        let row=prod;
        const table1 = {
            
            headers: ['Name','Date' ,'quatity', 'unit','unit Price','vat','total'],
            
            rows: row
        };
        
        doc.moveDown().fontSize(12).table(table1,  {
            prepareHeader: () => doc.font('Helvetica-Bold'),
            prepareRow: (row, i) => doc.font('Helvetica').fontSize(12)
        },20, 300, { width: 450 });
        let size=300+(product.length*50);
        doc.fontSize(10).font('Helvetica-Bold').text("total net :"+req.body.net, 400, size);
        doc.fontSize(10).font('Helvetica-Bold').text("total amount due:"+req.body.totalTTC, 400, size+30);
        doc.fontSize(10).text("payment method"+req.body.payment.paymentMeans, 100, size+60);
        doc.fontSize(10).text(req.body.payment.paypal, 150, size+80);
        doc.fontSize(10).text(req.body.notesFooter, 200, size+100);

doc.end();
            res.status(200).json({ message: "Devis updated" });
        })

    },
    delete: (req, res)=>{
        const DevisId = req.params.id;
        Devis
        .findByIdAndDelete(DevisId, { $set: { is_active: false } }).exec((err, DevisDetails) => {
            if (err) res.status(500).json({message : err})
            fs.unlink(`${__dirname}/uploads/pdf_Files/${DevisId}.pdf`, (err) => {
                if (err) console.log("esstimate not found");;
                console.log('path/file.pdf was deleted');
              });
            res.status(200).json({ message: "Devis Deleted" });
        })
    }, 
    convertEstimate:(req, res) => {
        const id = req.params.id;
        Devis
        .findByIdAndDelete(id, { $set: { is_active: false } }).exec((err, DevisDetails) => {
            if (err) res.status(500).json({message : err})
        })       
        Bill.create(req.body, (err, BillDetails) => {
            if (err) {
                console.error(err);
                res.status(500).json({message : err})
            }
            fs.unlink(`${__dirname}/uploads/pdf_Files/${id}.pdf`, (err) => {
                if (err) console.log("esstimate not found");;
                console.log('path/file.pdf was deleted');
              });
        doc.pipe(fs.createWriteStream(`api/bills/uploads/pdf_Files/${BillDetails._id}.pdf`));
        doc.image(`api/companies/uploads/${req.body.logo}`, {
            fit: [80,80],
         });
        doc.fontSize(19).font('Helvetica-Bold').text(req.body.companyName, 300, 80);
        doc.fontSize(15).text("date created :"+req.body.billDate, 300, 120);
        doc.fontSize(15).text("due date :"+req.body.deadline, 300, 160);
        doc.fontSize(15).text(req.body.message, 100, 250);     
        let prod=[];
        let product=req.body.products;
        for(let i=0;i<product.length;i++){
            prod[i]=[product[i].name,product[i].dateProd,product[i].quatity,product[i].unit,product[i].priceUnit,product[i].vat,product[i].priceHT];
        }
        let row=prod;
        const table1 = {
            
            headers: ['Name','Date' ,'quatity', 'unit','unit Price','vat','total'],
            
            rows: row
        };
        
        doc.moveDown().fontSize(12).table(table1,  {
            prepareHeader: () => doc.font('Helvetica-Bold'),
            prepareRow: (row, i) => doc.font('Helvetica').fontSize(12)
        },20, 300, { width: 450 });
        let size=300+(product.length*50);
        doc.fontSize(10).font('Helvetica-Bold').text("total net :"+req.body.net, 400, size);
        doc.fontSize(10).font('Helvetica-Bold').text("total amount due:"+req.body.totalTTC, 400, size+30);
        doc.fontSize(10).text("payment method"+req.body.payment.paymentMeans, 100, size+60);
        doc.fontSize(10).text(req.body.payment.paypal, 150, size+80);
        doc.fontSize(10).text(req.body.notesFooter, 200, size+100);

doc.end();
      res.status(201).json({ message: "Bill Created Successfully", data : BillDetails});
    });
},
    createpdf:(req, res) => {
        console.log(req.body);
        Devis.create(req.body, (err, DevisDetails) => {
            
            if (err) {
                console.error(err);
                res.status(500).json({message : err})
            }

        doc.pipe(fs.createWriteStream(`${__dirname}/uploads/pdf_Files/${DevisDetails._id}.pdf`));
        doc.image(`api/companies/uploads/${req.body.logo}`, {
            fit: [80,80],
         });
        doc.fontSize(19).font('Helvetica-Bold').text(req.body.companyName, 300, 80);
        doc.fontSize(15).text("date created :"+req.body.devisDate, 300, 120);
        doc.fontSize(15).text("due date :"+req.body.dateValidity, 300, 160);
        doc.fontSize(15).text(req.body.message, 100, 250);     
        let prod=[];
        let product=req.body.products;
        for(let i=0;i<product.length;i++){
            prod[i]=[product[i].name,product[i].dateProd,product[i].quatity,product[i].unit,product[i].priceUnit,product[i].vat,product[i].priceHT];
        }
        let row=prod;
        const table1 = {
            
            headers: ['Name','Date' ,'quatity', 'unit','unit Price','vat','total'],
            
            rows: row
        };
        
        doc.moveDown().fontSize(12).table(table1,  {
            prepareHeader: () => doc.font('Helvetica-Bold'),
            prepareRow: (row, i) => doc.font('Helvetica').fontSize(12)
        },20, 300, { width: 450 });
        let size=300+(product.length*60);
        doc.fontSize(10).font('Helvetica-Bold').text("total net :"+req.body.net, 400, size);
        doc.fontSize(10).font('Helvetica-Bold').text("total amount due:"+req.body.totalTTC, 400, size+30);
        doc.fontSize(10).text("payment method"+req.body.payment.paymentMeans, 100, size+60);
        doc.fontSize(10).text(req.body.payment.paypal, 150, size+80);
        doc.fontSize(10).text(req.body.notesFooter, 200, size+100);

doc.end();
      res.status(201).json({ message: "Devis Created Successfully", data : DevisDetails});
    });
},
       fetchpdf:(req, res) => {
           let id=req.params.id;
        res.sendFile(`${__dirname}/uploads/pdf_Files/${id}.pdf`);
      }
}