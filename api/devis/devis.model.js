'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const DevisSchema = new Schema({
    id_user: {
        type: String,
        required: true
    },
    id_client: {
        type: String,
        required: true
    },
    companyName: {
        type: String,
        required: true
    },
    devisNumber: {
        type: Number,
        required: true
    },
    devisDate: {
        type: Date,
        required: true
    },
status:{
 type:String,
 required:true
},
 dateValidity: {
        type: Date,
        required: true
    },
    message:{
        type:String,
        required:false
    },
    notesFooter : {
        type: String,
       required: true
    },
    discount : {
        type: Number,
        required:false
    },
    totalTTC: {
        type: Number,
        required:true
    },
    net: {
        type: Number,
        required:true
    },
    products:[
        {
            id_product:String,
            name:String,
            date:Date,
            quatity:Number,
            unit:String,
            priceUnit:Number,
            vat:Number,
            price:Number,
            priceHT:Number
        }
    ]
},{
    id: false,
    toObject: {
        virtuals: true,
        getters: true
    },
    toJSON: { 
        virtuals: true,
        getters: true, 
        setters: false 
    },
    timestamps: true
});

DevisSchema.pre('find', function () {
    this.where({ is_active: { $ne: false } });
});

module.exports = mongoose.model('Devis', DevisSchema);