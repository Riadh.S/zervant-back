'use strict';

const express = require('express');
const controller = require('./devis.controller');
const router = express.Router();
const VerifyToken = require('../auths/verifyToken');
router.get('/fetch-pdf/:id',controller.fetchpdf);
router.get('/', VerifyToken, controller.index);
router.get('/:id', VerifyToken, controller.retrieveByUser);
router.get('/:id', VerifyToken, controller.retrieve);
router.delete('/:id', VerifyToken, controller.delete);
router.post('/create-pdf',VerifyToken,controller.createpdf);
router.put('/update-pdf/:id', VerifyToken, controller.updatePdf);
router.post('/convert-estimate/:id',VerifyToken,controller.convertEstimate);
router.put('/:id', VerifyToken, controller.update);
module.exports = router;