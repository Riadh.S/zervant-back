'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const DevisSettingSchema = new Schema({
    id_user:{
        type: String,
        required: true
    },
    companyName:{
        type: String,
        required: false 
    },    
    currency:{
        type: String,
        default:'EUR' 
    },
    language:{
        type: String,
        default:'English'  
    },
    devisHeader: {
        type: String,
        required: true
    },
    dayValidity: {
      
        type: Boolean,
        default:false
    },
   attentionOf:{
    type: Boolean,
    default: false 
    },
    displayColunms:
    [
       {label:String,
       value:{type:Boolean,
       default:true
    }}
    ],
    discountSetting:[
        {label:String,
            value:{type:Boolean,
            default:true
         }}
        ],
        displayDS:{
             type:Boolean,
             default:true
         }  ,
    message:{
        type: String,
        required: false 
    },
    notesFooter:{
        type: String,
        required: false 
    },
    ConditionsSale:{
        valueCS:{
            type: String,
            required: false 
        },
        displayCS:{
            type:Boolean,
            default:true  
        }
    }
    
},{
    id: false,
    toObject: {
        virtuals: true,
        getters: true
    },
    toJSON: { 
        virtuals: true,
        getters: true, 
        setters: false 
    },
    timestamps: true
});

DevisSettingSchema.pre('find', function () {
    this.where({ is_active: { $ne: false } });
});

module.exports = mongoose.model('DevisSetting', DevisSettingSchema);