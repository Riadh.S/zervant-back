const Email=require("./emails.model");
const nodemailer = require("nodemailer");
module.exports={
create:(req,res)=>{
    Email.create(req.body, (err, EmailDetails) => {
        if (err) {
            console.error(err);
            res.status(500).json({message : err})
        }
        var transporter = nodemailer.createTransport({
            service: 'gmail',
            auth: {
                   user: 'Licht4231@gmail.com',
                   pass: 'Licht:1234'
               }
           });
      
          let mailOptions="";
          if(req.body.typeAttachment==="invoice"){ 
          mailOptions= {
            from: req.body.from, 
            to: req.body.to,
            subject: req.body.subject, 
            text: req.body.message,
            attachments: [{
                filename: 'invoice.pdf',
                path: `api/bills/uploads/pdf_Files/${req.body.id}.pdf`,
                contentType: 'application/pdf'
              }]
          };
        }
        if(req.body.typeAttachment==="estimate"){ 
            mailOptions= {
              from: req.body.from, 
              to: req.body.to,
              subject: req.body.subject, 
              text: req.body.message,
              attachments: [{
                  filename: 'estimate.pdf',
                  path: `api/devis/uploads/pdf_Files/${req.body.id}.pdf`,
                  contentType: 'application/pdf'
                }]
            };
          }
          transporter.sendMail(mailOptions, function (err, info) {
            if(err)
            res.status(500).json({ message: err});
        res.status(201).json({ message: "email Send Successfully", data : EmailDetails});
    });
});
},
retrieve: (req, res) => {
    const DevisId = req.params.id;
     Email
     .findOne({_id:DevisId})
     .exec((err, EmailsDetails)=>{
         if (err) {
             console.error(err);
             res.status(500).json({message : err})
         }
         res.status(200).json({ message: "Email Detail fetched Successfully", data : EmailsDetails});
     })
 },
 retrieveByUser: (req, res) => {
     const IdUser = req.params.id;
     Email
     .find({id_user:IdUser})
     .exec((err, EmailDetails)=>{
         if (err) {
             console.error(err);
             res.status(500).json({message : err})
         }
         res.status(200).json({ message: "Emails Detail fetched Successfully", data : EmailDetails});
     })
 },
}