'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const EmailSchema = new Schema({
    id_user:{
        type: String,
        required: true
    },
    from:{
        type:String,
        required:true
    },
    to:{
      type:String,
      required:true
    },
  subject:{
      type:String,
      required:true
  },
  message:{
    type:String,
    required:true
},
attachement:{
    type:String,
    required:true
},
date:{
    type:Date,
    required:true
},
typeAttachment:{
    type:String,
    required:true
},

},{
    id: false,
    toObject: {
        virtuals: true,
        getters: true
    },
    toJSON: { 
        virtuals: true,
        getters: true, 
        setters: false 
    },
    timestamps: true
});
module.exports = mongoose.model('Email', EmailSchema);