'use strict';

const express = require('express');
const controller = require('./emails.controller');
const router = express.Router();
const VerifyToken = require('../auths/verifyToken');
router.get('/:id', VerifyToken, controller.retrieveByUser);
router.get('/:id', VerifyToken, controller.retrieve);
router.post('/',VerifyToken,controller.create);
module.exports = router;