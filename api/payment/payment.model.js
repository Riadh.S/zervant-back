'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const PaymentSchema = new Schema({
    id_user:{
        type: String,
        required: true
    },
    paymentMeans: {
        type: String,
        required: true
    },
    paypal: {
        type: String,
        required: true
    },
    bancData:{
    bancDATA: {
        type: String,
        required: true
    },
    swiftCode: {
        type: String,
        required: true
    },
   IBAN:{
        type: String,
        required: false 
    }
},
   RIB:{
    bancRIB:{
    type: Number,
    required: false 
    },
    codeBox:{
        type: String,
        required: false 
    },
    accountNumber:{
        type: Number,
        required: false 
    },
    accountHolder:{
        type: String,
        required: false 
    }
}
},{
    id: false,
    toObject: {
        virtuals: true,
        getters: true
    },
    toJSON: { 
        virtuals: true,
        getters: true, 
        setters: false 
    },
    timestamps: true
});

PaymentSchema.pre('find', function () {
    this.where({ is_active: { $ne: false } });
});

module.exports = mongoose.model('Payment', PaymentSchema);