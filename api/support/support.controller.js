'use strict';
const server=require("../../app");
const multer = require("multer");
const Support = require('../support/support.model');
var io=require('socket.io')(server);
var storage = multer.diskStorage({
    destination:  `${__dirname}/uploads/`,
    filename: function(req, file, cb){
       cb(null,"ATTACHEMENT-" + Date.now() + file.originalname);
    }
 });
 
 var upload = multer({
    storage: storage
 }).single("myAttachement");
module.exports = {
    index: (req, res) => {
        Support
        .find({})
        .exec((err, SupportDetails)=>{
            if (err) {
                console.error(err);
                res.status(500).json({message : err})
            }
            res.status(200).json({ message: "Support Details fetched Successfully", data : SupportDetails});
        });
          
    },
    retrieve: (req, res) => {
        const SupportId = req.params.id;
        Support
        .findOne({_id:SupportId})
        .exec((err, SupportDetails)=>{
            if (err) {
                console.error(err);
                res.status(500).json({message : err})
            }
            res.status(200).json({ message: "Support Detail fetched Successfully", data : SupportDetails});
        })
    },
    retrieveByUser: (req, res) => {
        const IdUser = req.params.id;
        Support
        .find({id_user:IdUser})
        .exec((err, SupportDetails)=>{
            if (err) {
                console.error(err);
                res.status(500).json({message : err})
            }
            res.status(200).json({ message: "Support Detail fetched Successfully", data : SupportDetails});
        })
    },
    create: (req, res) => {
        
        Support.create(req.body, (err, SupportDetails) => {
            if (err) {
                console.error(err);
                res.status(500).json({message : err})
            }
            io.emit('message',SupportDetails);
            res.status(201).json({ message: "Support Created Successfully", data : SupportDetails});
        });
    },
    update: (req, res)=>{
        const SupportId = req.params.id;
        Support
        .findByIdAndUpdate(SupportId, { $set: req.body }).exec((err, SupportDetails) => {
            if (err) res.status(500).json({message : err})
           if(req.body.typeUpdate==="status"){
               console.log("emit");
              let data={
                id:SupportId,
                status:req.body.status_support  
              }; 
            io.emit('updateStatus',data);  
           }
           if(req.body.typeUpdate==="messageAdmin"){
            console.log("messageAdmin");
            console.log(req.body.typeUpdate);
           let data={
             id:SupportId,
             message:req.body.messages  
           }; 
         io.emit('responseAdmin',data);  
        }
        if(req.body.typeUpdate==="messageUser"){
            console.log("messageUser");
            console.log(req.body.typeUpdate);
           let data={
             id:SupportId,
             message:req.body.messages  
           }; 
         io.emit('responseUser',data);  
        }  
            res.status(200).json({ message: "Support updated",data:SupportDetails });
        })
    },
    delete: (req, res)=>{
        const SupportId = req.params.id;
        Support
        .findByIdAndDelete(SupportId, { $set: { is_active: false } }).exec((err, SupportDetails) => {
            if (err) res.status(500).json({message : err})
            res.status(200).json({ message: "Support Deleted" });
        })
    },
    uploadImage:(req, res) =>{
        const SupportId = req.params.id;
        const indexSup=req.params.index;
        upload(req, res, function (err) 
        {
            Support
            .findOne({_id:SupportId})
            .exec((err, SupportDetail)=>{
                if(err) res.status(500).json({message : err})
               let msgs=SupportDetail.messages;           
                let msg=msgs[indexSup];
             msg.attachement=req.file.filename;
             msgs[indexSup]=msg;   
            Support
            .findByIdAndUpdate(SupportId, { $set:{"messages":msgs} }).exec((err, SupportDetails) => {
                if (err) res.status(500).json({message : err})
            });
            });
            if (err instanceof multer.MulterError) {
                return res.status(500).json(err)
            } else if (err) {
                return res.status(500).json(err)
            }
       return res.status(200).send(req.file)
    
     }) 
},
getImage:(req,res)=>{
    let id=req.params.id;
    console.log(id);
    res.sendFile(`${__dirname}/uploads/${id}`);
} 
}