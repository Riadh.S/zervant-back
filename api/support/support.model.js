'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const SupportSchema = new Schema({
    id_user:{
        type: String,
        required: true
    },
    emailUser:{
        type:String,
        required:true
    },
    subject:{
      type:String,
      required:true
    },
    messages:[
        {
            message:String,
            date:Date,
            attachement:String,
            status:String,
            typeUser:String
        }
    ],
  status_support:{
      type:String,
      required:true
  }

},{
    id: false,
    toObject: {
        virtuals: true,
        getters: true
    },
    toJSON: { 
        virtuals: true,
        getters: true, 
        setters: false 
    },
    timestamps: true
});

SupportSchema.pre('find', function () {
    this.where({ is_active: { $ne: false } });
});

module.exports = mongoose.model('Support', SupportSchema);