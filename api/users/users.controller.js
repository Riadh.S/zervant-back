"use strict";

const User = require("../users/users.model");
const jwt = require("jsonwebtoken");
const config = require("../../config/environment");
const server=require("../../app");
const io=require("socket.io")(server);
module.exports = {
    index: (req, res) => {
       User
        .find({})
        .exec((err,UserDetails)=>{
            if (err) {
                console.error(err);
                res.status(500).json({message : err})
            }
            res.status(200).json({ message: "User Details fetched Successfully", data :UserDetails});
        })
    },
    retrieve: (req, res) => {
        const UserId = req.params.id;
       User
        .findOne({_id:UserId})
        .exec((err,UserDetails)=>{
            if (err) {
                console.error(err);
                res.status(500).json({message : err})
            }
            res.status(200).json({ message: "User Detail fetched Successfully", data :UserDetails});
        })
    },
     async create(req, res) {
        console.log(req.body);
        let test=false;
        const result=await User.findOne({email : req.body.email});
                if(!result){
                    test=false;
                }else{
                    test=true;
                }
       if(test===true){            
                 return res.status(201).json({message : "Email already exist"});
            }else{ 
       User.create(req.body, (err,UserDetails) => {
            if (err) {
                console.error(err);
                res.status(500).json({message : err})
            }else{
                let payload = {
                    user_id : UserDetails._id,
                    email:UserDetails.email
                }
                let token = jwt.sign(payload, config.secrets.session,{
                    expiresIn : config.secrets.expiresIn
                });
          
                return res.status(200).json({auth : true, token : token, message : "User Logged In Successfully"});  
            }
          
        })
    }
    },
    update: (req, res)=>{
        const UserId = req.params.id;
       User
        .findByIdAndUpdate(UserId, { $set: req.body }).exec((err,UserDetails) => {
            if (err) res.status(500).json({message : err})
            res.status(200).json({ message: "User updated" });
        })
    },
    delete: (req, res)=>{
        const UserId = req.params.id;
       User
        .findByIdAndDelete(UserId, { $set: { is_active: false } }).exec((err,UserDetails) => {
            if (err) res.status(500).json({message : err})
            res.status(200).json({ message: "User Deleted" });
        })
    }
}
