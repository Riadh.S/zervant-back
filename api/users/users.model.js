"use strict";

const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const DEFAULTPASSWORD = "adminuser";
const bcrypt = require("bcryptjs");

const UserSchema = new Schema(
  {
    name: {
      type: String,
      required: false
    },
    email: {
      type: String,
      required: true
    },
    country: {
      type: String,
      required: false
    },
    password: {
      type: String,
      required: true
    },
    companyName: {
      type: String,
      required: true
    },
    currency: {
      type: String,
      required: false
    },
    language: {
      type: String,
      required: false
    },
    is_active: {
      type: Boolean,
      default: true
    },
    is_admin: {
      type: Boolean,
      default: false
    }
  },
  {
    id: false,
    toObject: {
      virtuals: true,
      getters: true
    },
    toJSON: {
      virtuals: true,
      getters: true,
      setters: false
    },
    timestamps: true
  }
);

UserSchema.pre("find", function() {
  this.where({ is_active: { $ne: false } });
});

var validatePresenceOf = function(value) {
  return value && value.length;
};

/**
 * Pre-save hook
 */
UserSchema.pre("save", function(next) {
  if (!this.isNew) return next();

  if (!validatePresenceOf(this.password)) {
    next();
  } else {
    this.newPassword = this.password || DEFAULTPASSWORD;
    this.password = bcrypt.hashSync(this.newPassword, 8);
    return next();
  }
});

module.exports = mongoose.model("User", UserSchema);
