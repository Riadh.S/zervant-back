/**
 * Main application file
 */

"use strict";

// Set default node environment to development
process.env.VENDOR_NODE_ENV = process.env.VENDOR_NODE_ENV || "development";

const express = require("express");
const config = require("./config/environment");
const mongoose = require("mongoose");
const helmet=require("helmet");
//Connect to database
mongoose.Promise = global.Promise;
mongoose.connect(config.db.URI, config.mongo.options);
console.log("mongo connected");
mongoose.connection.on("error", function(err) {
  console.error("MongoDB connection error: " + err);
});
const app = express();
let server = require("http").createServer(app);
module.exports=server;
app.use(helmet());
var io = require('socket.io')(server);
require("./config/express")(app);
require("./routes")(app);
require("./config/seed");
server.listen(config.port, config.ip, function() {
  console.log(
    "Express server listening on %d, in %s mode",
    config.port,
    app.get("env")
  );
});
module.exports = app;
