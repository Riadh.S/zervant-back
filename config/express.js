/**
 * Express configuration
 */

'use strict';

const express = require('express');
const morgan = require('morgan');
const compression = require('compression');
const bodyParser = require('body-parser');
const cors = require('cors');
const path = require('path');
const config = require('./environment');
module.exports = function(app) {
  const env = app.get('env');

  // **ALLOW CORS**** WARN: NOT SECURE 
  app.use(cors());
  app.use(compression());
  app.use(bodyParser.urlencoded({ extended: false }));
  app.use(bodyParser.json());
  
  if ('development' === env) {
    app.use(morgan('dev'));
  }
};