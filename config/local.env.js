'use strict';

module.exports = {

    secrets: {
        session: "application-erp",
        expiresIn: 68400
    },

    db:{
        URI: 'mongodb://localhost:27017/application_db'
    }
}