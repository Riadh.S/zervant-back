/**
 * Main application routes
 */

"use strict";
module.exports = function(app) {
  app.use("/api/auths", require("./api/auths"));
  app.use("/api/companies", require("./api/companies"));
  app.use("/api/products", require("./api/products"));
  app.use("/api/customers", require("./api/customers"));
  app.use("/api/bills", require("./api/bills"));
  app.use("/api/devis", require("./api/devis"));
  app.use("/api/payment", require("./api/payment"));
  app.use("/api/bill_setting", require("./api/bill_setting"));
  app.use("/api/devis_setting", require("./api/devis_setting"));
  app.use("/api/users", require("./api/users"));
  app.use("/api/support", require("./api/support"));
  app.use("/api/email", require("./api/emails"));
};
